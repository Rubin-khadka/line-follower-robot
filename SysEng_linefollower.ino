

#include <Arduino.h>


#if !defined ESP32
#error Wrong board selected
#endif

#define CAMERA_MODEL_AI_THINKER

#include "esp_camera.h"       ///< Header file for camera obtained from https://github.com/espressif/

#include "driver/ledc.h"      ///< To enable onboard Illumination/flash LED pin attached on 4

#include "soc/soc.h"          //! Used to disable brownout detection
#include "soc/rtc_cntl_reg.h" //! Used to disable brownout detection

#include "WiFi.h"
#include <WiFiClient.h>
#include <WebServer.h>
#include <SPIFFS.h>
#include <FS.h>




//****************************************************************
// defines
//****************************************************************

#if defined(CAMERA_MODEL_AI_THINKER)
#define PWDN_GPIO_NUM 32
#define RESET_GPIO_NUM -1
#define XCLK_GPIO_NUM 0
#define SIOD_GPIO_NUM 26
#define SIOC_GPIO_NUM 27
#define Y9_GPIO_NUM 35
#define Y8_GPIO_NUM 34
#define Y7_GPIO_NUM 39
#define Y6_GPIO_NUM 36
#define Y5_GPIO_NUM 21
#define Y4_GPIO_NUM 19
#define Y3_GPIO_NUM 18
#define Y2_GPIO_NUM 5
#define VSYNC_GPIO_NUM 25
#define HREF_GPIO_NUM 23
#define PCLK_GPIO_NUM 22
#endif



#define SAVED_FILE "/image.jpg"            ///< Image File Name for SPIFFS

#define PIXFORMAT PIXFORMAT_GRAYSCALE                         // image format


#define N_PIXELS                  96


//****************************************************************
// global variables
//****************************************************************

const framesize_t FRAME_SIZE_IMAGE = FRAMESIZE_96X96;


int cameraImageExposure = 0;                // Range: (0 - 1200) 0:auto

int cameraImageGain = 0;                    // Range: (0 - 30) 0:auto

const uint8_t ledPin = 4;                  ///< onboard Illumination/flash LED pin (4)
unsigned int ledBrightness = 30;            ///< Initial brightness (0 - 255)
const int pwmFrequency = 50000;            ///< PWM settings for ESP32
const uint8_t ledChannel = LEDC_CHANNEL_0; ///< Camera timer0
const uint8_t pwmResolution = 8;           ///< resolution (8 = from 0 to 255)

const int serialSpeed = 115200;            ///< Serial data speed to use

boolean captureNewImage = false;



uint8_t line_of_interest_a[96];
uint8_t line_of_interest_b[96];


// state
bool state_run = false;



/**************************************************************************/
// WIfi credentials and webserver init
const char* ssid = "follownet";
const char* password = "walktheline";
WebServer server(80);
String checkMsg = "";

/**************************************************************************/
/**
  Camera Image Settings
  Set Image parameters
  Based on CameraWebServer sample code by ESP32 Arduino
  \return true: successful, false: failed
*/
/**************************************************************************/
bool cameraImageSettings()
{

  sensor_t *s = esp_camera_sensor_get();
  if (s == nullptr)
  {
    Serial.println("Error: problem reading camera sensor settings");
    return false;
  }

  // if both set to zero enable auto adjust
  if (cameraImageExposure == 0 && cameraImageGain == 0)
  {
    // enable auto adjust
    s->set_gain_ctrl(s, 1);     // auto gain on
    s->set_exposure_ctrl(s, 1); // auto exposure on
    s->set_awb_gain(s, 1);      // Auto White Balance enable (0 or 1)
    s->set_hmirror(s, 1);
    s->set_vflip(s, 1);
  }
  else
  {
    // Apply manual settings
    s->set_gain_ctrl(s, 0);                   // auto gain off
    s->set_awb_gain(s, 1);                    // Auto White Balance enable (0 or 1)
    s->set_exposure_ctrl(s, 0);               // auto exposure off
    s->set_agc_gain(s, cameraImageGain);      // set gain manually (0 - 30)
    s->set_aec_value(s, cameraImageExposure); // set exposure manually  (0-1200)
  }

  return true;
}

/**************************************************************************/
/**
  Initialise Camera
  Set camera parameters
  Based on CameraWebServer sample code by ESP32 Arduino
  \return true: successful, false: failed
*/
/**************************************************************************/
bool initialiseCamera()
{
  camera_config_t config;
  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sccb_sda = SIOD_GPIO_NUM;
  config.pin_sccb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 20000000; // 20000000 ori //new 1000000
  config.pixel_format = PIXFORMAT;
  config.frame_size = FRAME_SIZE_IMAGE;
  config.jpeg_quality = 10;
  config.fb_count = 1;

  // Check the esp32cam board has a PSRAM chip installed (extra memory used for storing captured images)
  // Note: if not using "AI thinker esp32 cam" in the Arduino IDE, PSRAM must be enabled
  if (!psramFound())
  {
    Serial.println("Warning: No PSRam found so defaulting to image size 'CIF'");
    config.frame_size = FRAMESIZE_CIF;
  }

  esp_err_t camera = esp_camera_init(&config); // initialise the camera
  if (camera != ESP_OK)
  {
    Serial.printf("ERROR: Camera init failed with error 0x%x", camera);
  }

  cameraImageSettings(); // Apply custom camera settings

  return (camera == ESP_OK); // Return boolean result of camera initialisation
}

/**************************************************************************/
/**
  Setup On Board Flash
  Initialize on board LED with pwm channel
*/
/**************************************************************************/
void setupOnBoardFlash()
{
  ledcSetup(ledChannel, pwmFrequency, pwmResolution);
  ledcAttachPin(ledPin, ledChannel);
}

/**************************************************************************/
/**
  Set Led Brightness
  Set pwm value to change brightness of LED
*/
/**************************************************************************/
void setLedBrightness(unsigned int &ledBrightness)
{
  unsigned int temp = map(ledBrightness, 0, 255, 0, 255);
  ledcWrite(ledChannel, temp);
}

void setup_motor_PWM(){

  ledcSetup(1, 1000, pwmResolution);
  ledcAttachPin(15, 1);

  ledcSetup(2, 1000, pwmResolution);
  ledcAttachPin(14, 2);

}

void set_speed(float speed, float turn){

  float speed_l = speed + turn;
  float speed_r = speed - turn;

  if (speed_l < 0){speed_l = 0;}
  if (speed_r < 0){speed_r = 0;}

  if (1 < speed_l){speed_l = 1;}
  if (1 < speed_r){speed_r = 1;}

  uint8_t motor_l = speed_l * 250;
  uint8_t motor_r = speed_r * 250;

  ledcWrite(1, motor_l);
  ledcWrite(2, motor_r);
}




//****************************************************************
// image processing

void extract_lines_of_interest(camera_fb_t *fb){

  for(uint8_t it = 0; it < N_PIXELS; it++){

    line_of_interest_a[it] = fb->buf[N_PIXELS*(48+20)             + it];
    line_of_interest_b[it] = fb->buf[N_PIXELS*(48-20)             + it];
    //line_of_interest_a[it] = fb->buf[N_PIXELS*it              + it];
    //line_of_interest_b[it] = fb->buf[N_PIXELS*(N_PIXELS-it-1) + it];
  }
}

void invert_lines_of_interest(void){

  for (uint8_t it = 0; it < N_PIXELS; it++){ line_of_interest_a[it] = 255 - line_of_interest_a[it];}
  for (uint8_t it = 0; it < N_PIXELS; it++){ line_of_interest_b[it] = 255 - line_of_interest_b[it];}
}

// position of the line
float center_of_mass_line(uint8_t *loi){

  uint32_t sum_center = 0;
  uint32_t sum_weight = 0;

  for(uint8_t it = 0; it < N_PIXELS; it++){

    sum_center += it * loi[it];
    sum_weight += loi[it];
  }
  return sum_center / (sum_weight * 48.0) - 1.0;        // value between -1 and 1
}

// is there a line
float abs_max_gradient_line(uint8_t *loi){

  int16_t max_diff = 0;

  for(uint8_t it = 0; it < N_PIXELS-1; it++){

    int16_t diff = abs(loi[it + 1] - loi[it]);
    if(max_diff < diff){max_diff = diff;}
  }

  return max_diff / 255.0;        // value between 0 and 1
}


//****************************************************************
// Webserver Setup
void handleRequest() {
  if (server.method() == HTTP_POST) {
    if (server.hasArg("plain")) {
      String message = server.arg("plain");
      //if (message == checkMsg){
      //  Serial.println(message + " pressed again !!!");
      //  server.send(400);
      //}
      if (message == "Start") {
        Serial.println("Start");
        checkMsg = "Start";
        server.send(200);
        state_run = true;
      }
      if (message == "Stop") {
        Serial.println("Stop");
        checkMsg = "Stop";
        server.send(200, "stop msg");
        state_run = false;
      }
    }
  }
}

void setupWiFi() {
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("Connecting to WiFi...");
  }
  Serial.println("Connected to WiFi");
  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());
}

void setupServer() {
  server.on("/", handleRequest);
  server.begin();
  Serial.println("Server Started");
}


//****************************************************************
// setup
//****************************************************************
void setup()
{
  Serial.begin(serialSpeed); ///< Initialize serial communication


  setup_motor_PWM();
  set_speed(0.01, 0);

  // webserver init
  setupWiFi();
  setupServer();

  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0); ///< Disable 'brownout detector'

  Serial.print("\nInitialising SPIFFS: "); ///< SPIFFS check
  if (SPIFFS.begin(true))
  {
    Serial.println("OK");
  }
  else
  {
    Serial.println("Error!");
    return;
  }


  Serial.print("\nInitialising camera: "); ///< Camera check
  if (initialiseCamera()){
    Serial.println("OK");
  }else{
    Serial.println("Error!");
    return;
  }


  setupOnBoardFlash();
  setLedBrightness(ledBrightness);


}

//****************************************************************
// main loop
//****************************************************************
void loop(){


  server.handleClient();

  camera_fb_t *fb = nullptr;  

  fb = esp_camera_fb_get();
  if (!fb){
    Serial.println("Camera capture failed");
    return;
  }

  // take two horizontal lines
  extract_lines_of_interest(fb);

  // invert them so darker regions become high values
  invert_lines_of_interest();

  // determine the center of mass and a sens of "is there a line"
  float line_position_a = center_of_mass_line(line_of_interest_a);
  float line_position_b = center_of_mass_line(line_of_interest_b);
  float max_gradient_a = abs_max_gradient_line(line_of_interest_a);
  float max_gradient_b = abs_max_gradient_line(line_of_interest_b);

  
  //Serial.println("----------------------");
  //Serial.println(line_position_a);
  //Serial.println(line_position_b);
  //Serial.println(max_gradient_a);
  //Serial.println(max_gradient_b);

 
  // adaptive steering
  float steering_amount = 1.0;
  float line_position;
  float max_gradient;

  if(max_gradient_a < max_gradient_b - 0.05){
    
    line_position = line_position_b;
    max_gradient = max_gradient_b;
    steering_amount = 1.1;
  }else{

    line_position = line_position_a;
    max_gradient = max_gradient_a;
    steering_amount = 0.8;
  }

  // driving in start-stop mode
  float speed = 0;
  static int8_t wait_cnt = 8;

  if(wait_cnt){wait_cnt--;}
  else{wait_cnt = 6;speed = 0.4;}


  // stop wiht delay
  static int8_t last_seen = 0;

  if(0.3 < max_gradient){
    last_seen = 50;
    //state_run = true;
  }else{
    if(0 < last_seen){last_seen--;}else{state_run = false;}
  }

  speed *= (uint8_t)state_run;

  set_speed(speed, line_position * 1);

  esp_camera_fb_return(fb);
}