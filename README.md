# SysEng_linefollower

this is the project work for the systems engineering<br>
done by<br>
**Rubin Khadka Chhetri**<br>
**Marco Ruchay**<br>
in FH Aachen <br>

**Goal of project** <br> 
  -Develop line-following capabilities for a 3-wheel robot using the ESP32-Cam.<br>
  -Integrate the ESP32-Cam into the robot platform and implement image processing.<br>
  -Create a wireless control system for starting and stopping the robot with onscreen buttons.<br>
  -Ensure accurate navigation of the robot along a non-straight line with turns and curves.<br>
  -Design a user-friendly onscreen dashboard interface for controlling and monitoring the robot's operation.<br>

**File Structure** <br>
--SysENg_linefollower.ino
  - contains the complete code done in arduino ide

**videos**
--folder contain two testing videos


